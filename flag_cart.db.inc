<?php
/**
 * @file
 * Database function for the Flag cart module.
 */

/**
 * Edit a specific flag cart block settings.
 *
 * @param array $data
 *   A array containing block settings.
 */
function flag_cart_edit_block($data) {
  if (flag_cart_get_block($data['flag_name'])) {
    $fc = db_update('flag_cart_block_settings')
      ->fields(array(
        'flag_url' => $data['flag_url'],
        'flag_icon' => $data['flag_icon'],
    ))
    ->condition('flag_name', $data['flag_name'], '=')
    ->execute();
  }
  else {
    $fc = db_insert('flag_cart_block_settings')
      ->fields(array(
          'flag_name' => $data['flag_name'],
          'flag_url' => $data['flag_url'],
            'flag_icon' => $data['flag_icon'],
      ))
      ->execute();
  }
}

/**
 * Get a specific flag cart block settings.
 *
 * @param string $flag_name
 *   Machine name of the block/flag.
 *
 * @return array
 *   Block settings.
 */
function flag_cart_get_block($flag_name) {
  $query = db_select('flag_cart_block_settings', 'fc')
      ->fields('fc')
      ->condition('flag_name', $flag_name, '=');

  $result = $query->execute();
  return $result->fetchObject();
}
