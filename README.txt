********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Flag cart
Author: Alex Arnaud <gg.alexarnaud@gmail.com>
Drupal: 7
********************************************************************

NOTE: This module extends Flag.

DESCRIPTION:

Flag cart extends Flag. It allows to create block with cart icons
that are automatically updated when content is flagged.
