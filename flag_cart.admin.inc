<?php
/**
 * @file
 * Admin function for the Flag cart module.
 */

/**
 * Form constructor for flag_cart settings.
 *
 * @see opac_admin_form_submit()
 * @ingroup forms
 */
function flag_cart_settings_form($form, &$form_state) {
  module_load_include('inc', 'flag_cart', "flag_cart.db");

  foreach (flag_get_flags() as $flag) {
    $flag_cart = flag_cart_get_block($flag->name);
    $form[$flag->name . '_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('@title block settings', array('@title' => $flag->title)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form[$flag->name . '_fieldset']['flag_url_' . $flag->name] = array(
      '#type' => 'textfield',
      '#title' => t('Cart url'),
      '#description' => t('url where to get a full view of the flag.'),
      '#default_value' => $flag_cart ? $flag_cart->flag_url : '',
      '#size' => 60,
      '#maxlength' => 128,
    );
    $form[$flag->name . '_fieldset']['flag_icon_' . $flag->name] = array(
      '#type' => 'textfield',
      '#title' => t('Cart icon'),
      '#description' => t('Path or url of the cart icon. i.e. /sites/default/images/icon.jpg'),
      '#default_value' => $flag_cart ? $flag_cart->flag_icon : '',
      '#size' => 60,
      '#maxlength' => 128,
    );
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Form submission handler for flag_cart_settings_form().
 */
function flag_cart_settings_form_submit($form, &$form_state) {
  module_load_include('inc', 'flag_cart', "flag_cart.db");
  $values = $form_state['values'];

  foreach (flag_get_flags() as $flag) {
    $data = array();
    $data['flag_url'] = $values['flag_url_' . $flag->name];
    $data['flag_icon'] = $values['flag_icon_' . $flag->name];
    $data['flag_name'] = $flag->name;
    flag_cart_edit_block($data);
  }
}
