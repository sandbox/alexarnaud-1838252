/**
 * Event handler that listens for flags selected by the user.
 */
jQuery(document).bind('flagGlobalAfterLinkUpdate', function(event, data) {
  var idElem = '#flag_cart_' + data.flagName;
  var url = "/flag_cart/update/" + data.flagName;

  jQuery.ajax({
    type: 'GET',
    url: url,
    success: function(data, textStatus, jqXHR) {
      jQuery(idElem).html(data);
    },
  });
});
